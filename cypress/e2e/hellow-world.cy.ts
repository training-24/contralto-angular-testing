describe('Cypress Page', () => {
  // ...
  beforeEach(() => {
    cy.viewport('iphone-8')
    cy.visit('https://fabiobiondi.dev')
  })

  it('should open the menu when hamburger is clicked', () => {
    cy.get('.hamburger-button').click()
    cy.get('.menu_item').contains('About').click()
    cy.url().should('include', '/about') // => true
  })

  it.only('first test', () => {
    expect('Fabio').to.not.eq('Mario')
    expect(1+2).to.eq(3)
  })
})
