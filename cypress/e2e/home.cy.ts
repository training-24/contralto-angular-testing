// cypress/e2e/login/login-page.cy.ts
import home from './home.page';

describe("Login", () => {
  beforeEach(() => {
    home.visit();
  })

  it('the SignIn button should be disabled by default', () => {
    home.isDisabled();
    // cy.screenshot()
  })

  it('the SignIn button should be disabled if validation fails', () => {
    home.fill('ma', '12')
    home.isDisabled();
  })

  it('the SignIn button should be enabled if validation is successful', () => {
    cy.login('mario', '12345')
    home.isEnabled();
  })

  it('visit home after login', () => {
    home.signIn('mario', '123445')
    // cy.login('mario', '12345', true)
    cy.url().should('include', '/uikit')
  })
});

