class HomePage {
  elements = {
    username: () => cy.get(`input[placeholder="Username"]`),
    password: () => cy.get(`input[placeholder="Password"]`),
    button: () => cy.contains('SIGN IN')
  }

  visit() {
    cy.visit('/home')
  }

  fill(u: string, p: string) {
    this.elements.username().type(u);
    this.elements.password().type(p);
  }

  isEnabled() {
    this.elements.button().should('be.enabled');
  }

  isDisabled() {
    this.elements.button().should('be.disabled');
  }

  signIn(u: string, p: string) {
    this.fill(u, p);
    this.elements.button().click()
  }
}

export default new HomePage()
