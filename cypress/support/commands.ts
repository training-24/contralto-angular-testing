/// <reference types="cypress" />

Cypress.Commands.add('login', (u: string, p: string, submit?: boolean) => {
  cy.get(`input[placeholder="Username"]`).type(u);
  cy.get(`input[placeholder="Password"]`).type(p);
  if (submit) {
    cy.contains('SIGN IN').click()
  }
})

declare namespace Cypress {
  interface Chainable {
    login(email: string, password: string, submit?: boolean): Chainable<void>
  }
}

// ***********************************************
// This example commands.ts shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
//

