import { Routes } from '@angular/router';

export const routes: Routes = [
  { path: 'home', loadComponent: () => import('./features/home.component')},
  { path: 'uikit', loadComponent: () => import('./features/uikit-demo.component')},
  { path: '', redirectTo: 'home', pathMatch: 'full'}
];
