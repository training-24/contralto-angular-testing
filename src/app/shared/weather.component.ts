// weather-signal.component.ts
import { Component, computed, effect, inject, input, signal } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Meteo } from '../model/meteo';

export const BASEURL = 'https://api.openweathermap.org/data/2.5/weather?units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534&q='

@Component({
  selector: 'app-weather',
  standalone: true,
  template: `
    @if (meteo()) {
      <h1>Weather in {{city()}}</h1>
      <pre>{{temperature()}}°</pre>
    }
  `,
})
export class WeatherComponent {
  http = inject(HttpClient)
  city = input<string>()
  meteo = signal<Meteo | null>(null);
  temperature = computed(() => this.meteo()?.main.temp)

  constructor() {
    effect(() => {
      const city = this.city();
      if (city) {
        this.http.get<Meteo>(`${BASEURL}${city}`)
          .subscribe(res => {
            this.meteo.set(res)
          })
      }

    });
  }

}
