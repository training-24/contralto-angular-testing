// shared/card
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';

/**
 *
 */
@Component({
  selector: 'app-card',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div>
      <h1
        class="title"
        (click)="isOpen = !isOpen"
      >
        {{title}}
        <div
          class="icon"
          *ngIf="icon"
          (click)="iconClickHandler($event)"
        >{{icon}}</div>
      </h1>

      <div
        class="body"
        *ngIf="isOpen"
      >
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: [`
    .title {
      @apply flex justify-between bg-slate-800 text-white p-3;
    }
    .body {
      @apply p-3 bg-slate-200;
    }
  `]
})
export class CardComponent {
  /**
   * ...
   */
  @Input() title: string | undefined;
  @Input() isOpen = true;
  @Input() icon: string | undefined;
  @Output() iconClick = new EventEmitter()

  iconClickHandler(event: MouseEvent) {
    event.stopPropagation();
    this.iconClick.emit()
  }
}
