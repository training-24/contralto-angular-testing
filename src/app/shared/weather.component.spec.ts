import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Meteo } from '../model/meteo';
import { BASEURL, WeatherComponent } from './weather.component';

describe('WeatherComponent', () => {
  let component: WeatherComponent;
  let httpMock: HttpTestingController;
  let fixture: ComponentFixture<WeatherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, WeatherComponent],
      providers: [
        // provideComponentStore(),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(WeatherComponent);
    component = fixture.componentInstance
    // component = TestBed.inject(WeatherComponent);

      httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('return the temperature', () => {
    const testResponse  = {
      main: {
        temp: 20
      }
    }
    const city = 'London'

    fixture.componentRef.setInput('city', city)
    fixture.detectChanges()

    const req = httpMock.expectOne(`${BASEURL}${city}`)
    req.flush(testResponse)
    fixture.detectChanges()

    expect(req.request.method).toEqual('GET')

    expect(fixture.nativeElement.querySelector('h1')!.textContent).toContain(`Weather in ${city}`)
    expect(fixture.nativeElement.querySelector('pre')!.textContent).toContain(`${testResponse.main.temp}°`)
  })

})
