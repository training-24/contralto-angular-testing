import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CardComponent } from './card.component';

describe('CardComponent', () => {
  let fixture: ComponentFixture<CardComponent>;
  let component: CardComponent;
  let el: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CardComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CardComponent);
    component =  fixture.componentInstance;
    el =  fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should be opened when mounted', () => {
    expect(component.isOpen).toBe(true)
  })
  it('should display the title correctly', () => {
    component.title = 'pippo';
    fixture.detectChanges()
    expect(el.querySelector('h1')?.textContent).toContain('pippo')
  })

  it('should hide content when header is clicked', () => {
     const titleBar: HTMLElement | null = el.querySelector('.title');
    const body = el.querySelector('.body');
    body!.innerHTML = 'lorem'

    titleBar?.click()
    fixture.detectChanges()

    expect(component.isOpen).toBe(false)
    expect(el.querySelector('.body')).toBeNull()
  })


  it('should hide content when header is clicked twice', () => {
     const titleBar: HTMLElement | null = el.querySelector('.title');
    const body = el.querySelector('.body');
    body!.innerHTML = 'lorem'

    titleBar?.click()
    titleBar?.click()
    fixture.detectChanges()

    expect(component.isOpen).toBe(true)
    expect(el.querySelector('.body')?.textContent).toContain('lorem')
  })


  it('should display the icon correctly', () => {
    component.icon = '💩';
    fixture.detectChanges()
    expect(el.querySelector('h1 .icon')?.textContent).toContain('💩')
  })


  it('should emit iconClick event when the icon is clicked', () => {
    spyOn(component.iconClick, 'emit')
    component.icon = '💩';
    fixture.detectChanges()

    const iconEl = el.querySelector('h1 .icon') as HTMLElement
    iconEl!.click()

    expect(component.iconClick.emit).toHaveBeenCalled()
    expect(component.iconClick.emit).toHaveBeenCalledTimes(1)
  })

  it('should emit iconClick event twice when the icon is clicked twice', () => {
    spyOn(component.iconClick, 'emit')
    component.icon = '💩';
    fixture.detectChanges()

    const iconEl = el.querySelector('h1 .icon') as HTMLElement
    iconEl!.click()
    iconEl!.click()

    expect(component.iconClick.emit).toHaveBeenCalledTimes(2)
  })



})



describe('Card Component | Content Projection', () => {
  let fixture: ComponentFixture<TestHostComponent>;
  let component: TestHostComponent;
  let el: HTMLElement;


  @Component({
    standalone: true,
    template: `
      <app-card>{{ content }}</app-card>
    `,
    imports: [CardComponent]
  })
  class TestHostComponent {
    @ViewChild(CardComponent) card!: CardComponent
    content = 'lorem ipsum'
  }


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CardComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestHostComponent);
    component =  fixture.componentInstance;
    el =  fixture.nativeElement;
    fixture.detectChanges();
  });


  it('should be opened when mounted', () => {
    expect(component.card.isOpen).toBe(true)
  })

  it('should render ng-content', () => {
    expect(el.querySelector('.body')!.textContent).toContain('lore')
  })

})
