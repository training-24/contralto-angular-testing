import { createOutputSpy, mount } from 'cypress/angular';
import { PanelComponent } from './panel.component';

describe('<Panel /> component', () => {

  it('should mount', function () {
    mount(PanelComponent)
  });

  it('should display title', function () {
    mount(PanelComponent, {
      componentProperties: {
        title: 'Hello'
      }
    })

    cy.contains('Hello').should('be.visible');
  });

  it('should display title and icon', function () {
    mount(PanelComponent, {
      componentProperties: {
        title: 'Hello',
        icon: '♥️'
      }
    })

    cy.contains('Hello').should('be.visible');
    cy.contains('♥️').should('be.visible');

  });


  it('should emit an event when icon is clicked', function () {
    mount(PanelComponent, {
      componentProperties: {
        title: 'Hello',
        icon: '♥️',
        iconClick: createOutputSpy('iconClickSpy')
      }
    })
    cy.contains('♥️').click()

    cy.get('@iconClickSpy').should('have.been.calledOnce')

  });




  it('should not display content when component is mount', function () {

    mount(
      `<app-panel title="ciao">lorem ipsum!!!</app-panel>`, {
        imports: [PanelComponent]
      }
    )

    cy.contains('️lorem ipsum').should('not.exist');
  });

  it('should  display content when titlebar is clicked', function () {

    mount(
      `<app-panel title="ciao">lorem ipsum!!!</app-panel>`, {
        imports: [PanelComponent]
      }
    )

    cy.contains('ciao').click()

    cy.contains('lorem ipsum').should('be.visible');
  });

})
