// model/user.ts
export interface User {
  id: number;
  name: string;
  username: string;
}
