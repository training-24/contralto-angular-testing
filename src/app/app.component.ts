import { Component } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';
import { CardComponent } from './shared/card.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, CardComponent, RouterLink],
  template: `
    <button routerLink="home">home</button>
    <button routerLink="uikit">uikit</button>
    <hr>
    <router-outlet />
  `,
  styles: [],
})
export class AppComponent {

}
