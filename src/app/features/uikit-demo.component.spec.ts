import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import UikitDemoComponent from './uikit-demo.component';

describe('UIKIT DEMO', () => {
  let fixture: ComponentFixture<UikitDemoComponent>;
  let component: UikitDemoComponent;
  let el: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UikitDemoComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(UikitDemoComponent);
    component =  fixture.componentInstance;
    el =  fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it(`should have the 'contralto-angular-testing' title`, () => {
    expect(component.title).toEqual('contralto-angular-testing');
  });


  it('should render title', fakeAsync (() => {
    tick();
    fixture.detectChanges();
    expect(el.querySelector('h1')?.textContent).toContain('Welcome to contralto-angular-testing!');
  }));

  it('should not render title if it is empty', () => {
    component.title = ''
    fixture.detectChanges();
    expect(el.querySelector('h1')?.textContent).toBe('');
  });
});

/*
export function select(selector: string, fixture: ComponentFixture<any>) {
  return fixture.nativeElement.querySelector(selector)
}
 */
