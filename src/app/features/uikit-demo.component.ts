import { Component } from '@angular/core';
import { CardComponent } from '../shared/card.component';
import { WeatherComponent } from '../shared/weather.component';

@Component({
  selector: 'app-uikit-demo',
  standalone: true,
  imports: [
    CardComponent,
    WeatherComponent
  ],
  template: `
    <!--<h1>UIKIT</h1>-->
    
    @if (title) {
      <h1>Welcome to {{title}}!</h1>
    } @else {
      <h1></h1>
    }

    <app-card>lorem ipsum</app-card>

    <app-card
      title="abc"
      [isOpen]="false"
      icon="♥️"
      (iconClick)="doSomething()"
    >
      lorem ipsum
    </app-card>
    
    <app-weather city="Milan" />
  `,
  styles: ``
})
export default class UikitDemoComponent {
  title = 'contralto-angular-testing';

  doSomething() {

  }
}
