// app.component
import { HttpClient } from '@angular/common/http';
import { Component, computed, inject, OnInit, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../model/user';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  template: `
    <form
      [formGroup]="form"
      (ngSubmit)="sendHandler()"
    >
      <input
        type="text"
        formControlName="username"
        placeholder="Username"
      />

      <input
        type="text"
        formControlName="password"
        placeholder="Password"
      />

      <button [disabled]="form.invalid" type="submit">
        SIGN IN
      </button>
    </form>

  `,
})
export default class HomeComponent  {
  fb = inject(FormBuilder)
  router = inject(Router)

  form = this.fb.group({
    username:[ '', { validators: [ Validators.required, Validators.minLength(3) ]}],
    password:[ '', { validators: [ Validators.required, Validators.minLength(3) ]}],
  })


  sendHandler() {
    this.router.navigateByUrl('uikit')
  }
}
